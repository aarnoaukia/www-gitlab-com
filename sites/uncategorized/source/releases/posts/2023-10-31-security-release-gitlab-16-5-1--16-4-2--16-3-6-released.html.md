---
title: "GitLab Security Release: 16.5.1, 16.4.2, 16.3.6"
categories: releases
author: Greg Alfaro
author_gitlab: truegreg
author_twitter: gitlab
description: "Learn more about GitLab Security Release: 16.5.1, 16.4.2, 16.3.6 for GitLab Community Edition (CE) and Enterprise Edition (EE)."
canonical_path: '/releases/2023/10/31/security-release-gitlab-16-5-1--16-4-2--16-3-6-released.html.md'
image_title: '/images/blogimages/security-cover-new.png'
tags: security
---

<!-- For detailed instructions on how to complete this, please see https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/patch/blog-post.md -->

Today we are releasing versions 16.5.1, 16.4.2, 16.3.6 for GitLab Community Edition (CE) and Enterprise Edition (EE).

These versions contain important security fixes, and we strongly recommend that all GitLab installations be upgraded to
one of these versions immediately. GitLab.com is already running the patched version.

GitLab releases patches for vulnerabilities in dedicated security releases. There are two types of security releases:
a monthly, scheduled security release, released a week after the feature release (which deploys on the 3rd Thursday of each month),
and ad-hoc security releases for critical vulnerabilities. For more information, you can visit our [security FAQ](https://about.gitlab.com/security/faq/).
You can see all of our regular and security release blog posts [here](/releases/categories/releases/).
In addition, the issues detailing each vulnerability are made public on our
[issue tracker](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=closed&label_name%5B%5D=bug%3A%3Avulnerability&confidential=no&first_page_size=100)
30 days after the release in which they were patched.

We are dedicated to ensuring all aspects of GitLab that are exposed to customers or that host customer data are held to
the highest security standards. As part of maintaining good security hygiene, it is highly recommended that all customers
upgrade to the latest security release for their supported version. You can read more
[best practices in securing your GitLab instance](/blog/2020/05/20/gitlab-instance-security-best-practices/) in our blog post.

On 2023-10-20 11:03 UTC, GitLab internally discovered (CVE-2023-5831) that a change in the GitLab sidebar feature resulted in self-managed GitLab instances sending version-checks to version.gitlab.com each time they opened a page on their GitLab instance. This means that the hostnames and current versions of self-managed GitLab instances were being sent to version.gitlab.com any time a user of that GitLab instance opened any page, regardless of whether or not the sending of version-check was enabled. This information was only accessible to some GitLab team members and was not exposed externally, and GitLab is working to purge the erroneously collected data from our database.

### Recommended Action

We **strongly recommend** that all installations running a version affected by the issues described below are **upgraded to the latest version as soon as possible**.

When no specific deployment type (omnibus, source code, helm chart, etc.) of a product is mentioned, this means all types are affected.

## Table of fixes

| Title | Severity |
| ----- | -------- |
| [Disclosure of CI/CD variables using Custom project templates](#disclosure-of-cicd-variables-using-custom-project-templates) | High |
| [GitLab omnibus DoS crash via OOM with CI Catalogs](#gitlab-omnibus-dos-crash-via-oom-with-ci-catalogs) | Medium |
| [Parsing  gitlab-ci.yml with large string via `timeout` input leads to Denial of Service](#parsing--gitlab-ciyml-with-large-string-via-timeout-input-leads-to-denial-of-service) | Medium |
| [DoS - Blocking FIFO files in Tar archives](#dos---blocking-fifo-files-in-tar-archives) | Medium |
| [Titles exposed by service-desk template](#titles-exposed-by-service-desk-template) | Medium |
| [Approval on protected environments can be bypassed](#approval-on-protected-environments-can-be-bypassed) | Low |
| [Version information disclosure when `super_sidebar_logged_out` feature flag is enabled](#version-information-disclosure-when-super_sidebar_logged_out-feature-flag-is-enabled) | Low |
| [Add abuse detection for search syntax filter pipes](#add-abuse-detection-for-search-syntax-filter-pipes) | Low |

### Disclosure of CI/CD variables using Custom project templates

An issue has been discovered in GitLab affecting all versions starting from 11.6 before 12.9.8, all versions starting from 12.10 before 12.10.7, all versions starting from 13.0 before 13.0.1. It was possible for an unauthorised project or group member to read the CI/CD variables using the custom project templates.
This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:C/C:H/I:L/A:N`, 8.5).
It is now mitigated in the latest release and is assigned [CVE-2023-3399](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-3399).

Thanks [theluci](https://hackerone.com/theluci) for reporting this vulnerability through our HackerOne bug bounty program.

### GitLab omnibus DoS crash via OOM with CI Catalogs

An issue has been discovered in GitLab CE/EE affecting all versions starting from 16.2 before 16.3.6, all versions starting from 16.4 before 16.4.2, all versions starting from 16.5 before 16.5.1. A low-privileged attacker can point a CI/CD Component to an incorrect path and cause the server to exhaust all available memory through an infinite loop and cause Denial of Service. This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H`, 6.5). It is now mitigated in the latest release and is assigned [CVE-2023-5825](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-5825).

Thanks [blakbat](https://hackerone.com/blakbat) for reporting this vulnerability through our HackerOne bug bounty program"

### Parsing  gitlab-ci.yml with large string via `timeout` input leads to Denial of Service

An issue has been discovered in GitLab CE/EE affecting all versions starting from 12.3 before 16.3.6, all versions starting from 16.4 before 16.4.2, all versions starting from 16.5 before 16.5.1. A Regular Expression Denial of Service was possible  by adding a large string in timeout input in gitlab-ci.yml file." This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:L`, 4.3). It is now mitigated in the latest release and is assigned [CVE-2023-3909](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-3909).

Thanks [akadrian](https://hackerone.com/akadrian) for reporting this vulnerability through our HackerOne bug bounty program.


### DoS - Blocking FIFO files in Tar archives

 An issue has been discovered in GitLab EE/CE affecting all versions starting before 16.3.6, all versions starting from 16.4 before 16.4.2, all versions starting from 16.5 before 16.5.1 which allows an attackers to block Sidekiq job processor. This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:L`, 4.3). It is now mitigated in the latest release and is assigned [CVE-2023-3246](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-3246).

Thanks [zhutyra](https://hackerone.com/zhutyra) for reporting this vulnerability through our HackerOne bug bounty program.


### Titles exposed by service-desk template

An issue has been discovered in GitLab EE affecting all versions starting from 16.0 before 16.3.6, all versions starting from 16.4 before 16.4.2, all versions starting from 16.5 before 16.5.1. Arbitrary access to the titles of an private specific references could be leaked through the service-desk custom email template. This is a low severity issue (`CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:U/C:L/I:N/A:N`, 3.1). It is now mitigated in the latest release and is assigned [CVE-2023-5600](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-5600).

Thanks [yvvdwf](https://hackerone.com/yvvdwf) for reporting this vulnerability through our HackerOne bug bounty program.


### Approval on protected environments can be bypassed

An authorization issue affecting GitLab EE affecting all versions from 14.7 prior to 16.3.6, 16.4 prior to 16.4.2, and 16.5 prior to 16.5.1, allowed a user to run jobs in protected environments, bypassing any required approvals. This is a low severity issue (`CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:C/C:L/I:N/A:N`, 3.5). It is now mitigated in the latest release and is assigned [CVE-2023-4700](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-4700).

Thanks [Gregor Pirolt](https://hackerone.com/gregodfather) for reporting this vulnerability through our HackerOne bug bounty program.


### Version information disclosure when `super_sidebar_logged_out` feature flag is enabled

An issue has been discovered in GitLab CE/EE affecting all versions starting from 16.0 before 16.3.6, all versions starting from 16.4 before 16.4.2, and all versions starting from 16.5.0 before 16.5.1 which have the `super_sidebar_logged_out` feature flag enabled. Affected versions with this default-disabled feature flag enabled may unintentionally disclose GitLab version metadata to unauthorized actors. This is a low severity issue (`CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:L/I:N/A:N`, 3.7).
It is now mitigated in the latest release and is assigned [CVE-2023-5831](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-5831).

This vulnerability was discovered internally by the GitLab team.


### Add abuse detection for search syntax filter pipes

An issue has been discovered in GitLab EE with Advanced Search affecting all versions from 13.9 to 16.3.6, 16.4 prior to 16.4.2 and 16.5 prior to 16.5.1 that could allow a denial of service in the Advanced Search function by chaining too many syntax operators. This is a low severity issue (`CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:U/C:N/I:N/A:L`, 3.1). It is now mitigated in the latest release. We have requested a CVE ID and will update this blog post when it is assigned.

This vulnerability was found internally by GitLab.

### Update curl to v8.4.0

curl has been updated to v8.4.0 to mitigate [CVE-2023-38545](https://curl.se/docs/CVE-2023-38545.html).

### Update mermaid to 10.5.0

mermaid has been updated to 10.5.0 to mitigate a security issue.

### Patch NGINX for CVE-2023-44487

NGINX has been patched to mitigate CVE-2023-44487.

## Non Security Patches

### 16.5.1

* [Revert better-error-messages-for-pull-mirroring](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/134835)
* [Update post migration to drop column only if it exists](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/134867)
* [Downgrade vue-apollo to prevent auto-restarting subscriptions on error](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/135226)

### 16.4.2

* [UBI: Explicitly add webrick gem to mailroom build](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/1554)
* [Update VERSION files](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/133882)
* [Update dependency prometheus-client-mmap to '>= 0.28.1'](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/133877)
* [Backport: fix migration when commit_message_negative_regex is missing](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/133833)
* [Backport to 16.4: Geo: Avoid getting resources stuck in Queued](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/134197)
* [Fix pipeline schedules view when owner is nil](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/134481)
* [Quarantine flaky delete_job_spec:46](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/134565)
* [Create Geo event when project is created](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/134264)
* [Fix bug with batched gitaly ref deletion duplicates](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/134328)

### 16.3.6

* [UBI: Explicitly add webrick gem to mailroom build](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/1555)
* [Backport 16.3: Upgrade exiftool to 12.65](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/1499)
* [Fixes the 16-3-stable branch](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/135401)
* [Backport to 16.3: Geo: Avoid getting resources stuck in Queued](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/134196)

## Updating

To update GitLab, see the [Update page](/update).
To update Gitlab Runner, see the [Updating the Runner page](https://docs.gitlab.com/runner/install/linux-repository.html#updating-the-runner).

## Receive Security Release Notifications

To receive security release blog notifications delivered to your inbox, visit our [contact us](https://about.gitlab.com/company/contact/) page.
To receive release notifications via RSS, subscribe to our [security release RSS feed](https://about.gitlab.com/security-releases.xml) or our [RSS feed for all releases](https://about.gitlab.com/all-releases.xml).
