---
layout: markdown_page
title: "GitLab Continues AI Momentum with Enhanced AI-Powered DevSecOps Workflows"
description: "GitLab Continues AI Momentum with Enhanced AI-Powered DevSecOps Workflows"
twitter_image: "/images/opengraph/Press-Releases/press-release-ai-momentum-duo.png"
twitter_creator: "@gitlab"
twitter_site: "@gitlab"
twitter_image_alt: "GitLab Continues AI Momentum with Enhanced AI-Powered DevSecOps Workflows"
---

_Introducing the beta availability of GitLab Duo Chat and the upcoming general availability of GitLab Duo Code Suggestions to increase security, efficiency, and collaboration across the entire software development lifecycle_

SAN FRANCISCO, November 9, 2023 - All Remote - GitLab Inc., ​​the most comprehensive AI-powered DevSecOps platform, today announced updates to GitLab Duo, the company’s suite of AI capabilities, including the beta of GitLab Duo Chat available in the GitLab 16.6 November product release, and the general availability of GitLab Duo Code Suggestions in the GitLab 16.7 December product release. 

GitLab Duo Chat, a powerful natural-language AI assistant, provides users with real-time guidance, insights, and suggestions to help analyze code, assist with planning, understand and fix security issues, troubleshoot CI/CD pipeline failures, assist with merge requests, and more. Duo Chat beta joins Code Suggestions as one of the two primary interfaces into GitLab’s AI-powered suite within its DevSecOps platform. 

[GitLab Duo](https://about.gitlab.com/gitlab-duo/) is a suite of 14 AI capabilities, including Suggested Reviewers, Code Suggestions, Chat, Vulnerability Summary, Code Explanation, Planning Discussions Summary, Merge Request Summary, Merge Request Template Population, Code Review Summary, Test Generation, Git Suggestions, Root Cause Analysis, Planning Description Generation, and Value Stream Forecasting.  

According to the [GitLab State of AI in Software Development](https://about.gitlab.com/developer-survey/#ai) report, developers spend only 25% of their time writing code. GitLab Duo reduces toolchain sprawl and optimizes the entire software development lifecycle, leading to 7x faster cycle times, better developer productivity, and reduced software spend. 

In addition, GitLab will make Code Suggestions generally available in the 16.7 December product release. Code Suggestions helps development, security, and operations teams create new code and update existing code to reduce cognitive load, improve efficiency, and enable them to build more secure software faster.

GitLab’s report also found that 83% of DevSecOps professionals said implementing AI in their software development processes is essential to avoid falling behind competitors, while 95% said they prioritize privacy and protection of intellectual property when selecting an AI tool.

[GitLab Duo](https://docs.gitlab.com/ee/user/ai_features.html) improves team collaboration and reduces the security and compliance risks of AI adoption by bringing the entire software development lifecycle into a single AI-powered application that is privacy and transparency first. This privacy-first approach helps provide the important information that customers need regarding GitLab Duo’s AI-assisted features and the ways in which data is securely used and protected.

To read more about how GitLab Duo protects customers’ proprietary data, read the blog [here](https://about.gitlab.com/blog/2023/11/09/gitlab-duo-chat-beta/).

**Supporting Quote**

* "The developers we speak with at RedMonk are keenly interested in the productivity and efficiency gains that code assistants promise. GitLab’s Duo Code Suggestions is a welcome player in this space, expanding the available options for enabling an AI-enhanced software development lifecycle," said Kate Holterhoff, industry analyst, Redmonk
* “The introduction of GitLab Duo Chat furthers our momentum and focus to bring AI beyond just code creation,” said David DeSanto, chief product officer, GitLab. “To realize AI’s full potential, it needs to be embedded across the software development lifecycle, allowing DevSecOps teams to benefit from boosts to security, efficiency, and collaboration. Organizations have never been more pressed to deliver innovation faster at lower costs while minimizing security risk, and we believe GitLab’s AI-powered DevSecOps Platform can help drive that efficiency.“

**About GitLab**

GitLab is the most comprehensive AI-powered DevSecOps platform for software innovation. GitLab enables organizations to increase developer productivity, improve operational efficiency, reduce security and compliance risk, and accelerate digital transformation. More than 30 million registered users and more than 50% of the Fortune 100 trust GitLab to ship better, more secure software faster.

**Media Contact**

Christina Weaver
<br>
press@gitlab.com
<br>