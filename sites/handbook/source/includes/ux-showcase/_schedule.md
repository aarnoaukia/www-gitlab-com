[//]: # TIP: Create the schedule in a temporary spreadsheet, and then copy/paste the rows into an online markdown generator (https://www.google.com/search?q=copy-table-in-excel-and-paste-as-a-markdown-table)

| Date       | Host                 | Presenter 1        | Presenter 2            | Presenter 3            |
|------------|----------------------|--------------------|------------------------|------------------------|
| 2023-11-15 | Jacki Bauer          | Michael Fangman    | Jeremy Elder           |                        |
| 2023-11-29 | Rayana Verissimo     | Sascha Eggenberger | Camellia Yang          |                        |
| 2023-12-13 | Jacki Bauer          | Amelia Bauerly     | Emily Bauman           |                        |
