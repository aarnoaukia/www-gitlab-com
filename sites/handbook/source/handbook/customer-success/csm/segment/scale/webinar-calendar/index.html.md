---
layout: handbook-page-toc
title: "CSM/CSE Webinar Calendar"
---
# On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [CSM Handbook homepage](/handbook/customer-success/csm/) for additional CSM/CSE-related handbook pages.

Watch previously recorded webinars on our [YouTube Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kpczt4pRtyF147Uvn2bGGvq).

---

# Upcoming Webinars

We’d like to invite you to our free upcoming webinars and workshops in the month of November.

If you know someone who might be interested in attending, feel free to share the registration links with them. Everyone is welcome, and we hope to see you there!

## November 2023

### AMER Time Zone Webinars & Workshops

#### Hands-On GitLab CI Workshop for Jenkins Users
##### November 15th, 2023 at 9:00-11:00AM Pacific Time / 12:00-2:00PM Eastern Time

Join us for a hands-on GitLab CI workshop and learn how it can fit in your organization!

We will kick things off by going over the differences between CI/CD in Jenkins and GitLab, syntax requirements, advantages to using GitLab, and how you can achieve the same outcomes in GitLab. Getting started with CI/CD in GitLab will take a lot less time than tends to be required for Jenkins, and your users can stay in a single platform. 

We will then dive into how to build simple GitLab pipelines and work up to more advanced pipeline structures and workflows, including security scanning and compliance enforcement.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_0DDpMMcjTJOS6Nhl-O-VbQ#/registration)

#### Advanced CI/CD
##### November 17th, 2023 at 9:00-10:00AM Pacific Time / 12:00-1:00PM Eastern Time

Expand your CI/CD knowledge while we cover advanced topics that will accelerate your efficiency using GitLab, such as pipelines, variables, rules, artifacts, and more. This session is intended for those who have used CI/CD in the past.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_LsTCwYMSSqOprA0hqJZrBQ#/registration)

#### Security and Compliance
##### November 21st, 2023 at 9:00-10:00AM Pacific Time / 12:00-1:00PM Eastern Time

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_xidq5sMsRxS3hMmIt_k5tQ#/registration)

#### Hands-on Workshop: Security and Compliance in GitLab
##### November 22nd, 2023 at 9:00-11:00AM Pacific Time / 12:00-2:00PM Eastern Time

In this workshop we will focus on how you can secure your application with GitLab. 

We will first take a look at how to apply scanners to your CICD pipelines in a hands-on exercise so that any vulnerabilities are caught as soon as the code is committed. 

Next, we will look at compliance frameworks and pipelines to show how you can ensure no one within your development teams is cutting corners and exposing your application.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_qlJgu1mITEe2GSOroGdP-w#/registration)

#### Getting Started with DevOps Metrics
##### November 28th, 2023 at 9:00-10:00AM Pacific Time / 12:00-1:00PM Eastern Time

Come learn about DevOps metrics in GitLab and why it is useful to track them. We will cover an overview of DORA metrics, Value Stream Analytics, and Insight dashboards, and what it looks like in Gitlab.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_B9bclrGFQ7ilqiIxJ0lc8A#/registration)

#### AI in DevSecOps - GitLab Hands-On Workshop
##### November 29th, 2023 at 9:00-10:30AM Pacific Time / 11:00AM-12:30PM Eastern Time

Join us for a hands-on GitLab AI workshop where we will explore Artificial Intelligence and how it fits within the DevSecOps lifecycle. In this session, we will cover foundational implementation and use case scenarios such as Code Suggestions and Vulnerability Explanations.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_AdwRl1yARfmFZ2owrEdWaQ#/registration)


### EMEA Time Zone Webinars and Workshops

#### Hands-On GitLab CI Workshop for Jenkins Users
##### November 15th, 2023 at 10:00AM-12:00PM UTC / 11:00AM-1:00PM CET

Join us for a hands-on GitLab CI workshop and learn how it can fit in your organization!

We will kick things off by going over the differences between CI/CD in Jenkins and GitLab, syntax requirements, advantages to using GitLab, and how you can achieve the same outcomes in GitLab. Getting started with CI/CD in GitLab will take a lot less time than tends to be required for Jenkins, and your users can stay in a single platform. 

We will then dive into how to build simple GitLab pipelines and work up to more advanced pipeline structures and workflows, including security scanning and compliance enforcement.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_HOvvRUlcT5u-GtJRz6ZwFQ#/registration)

#### Advanced CI/CD
##### November 21st, 2023 at 9:00-10:00AM UTC / 10:00-11:00AM CET

Expand your CI/CD knowledge while we cover advanced topics that will accelerate your efficiency using GitLab, such as pipelines, variables, rules, artifacts, and more. This session is intended for those who have used CI/CD in the past.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_yisBO6vWQNO_6A45RlS7Lg#/registration)

#### Hands-on Workshop: Security and Compliance in GitLab
##### November 22nd, 2023 at 10:00AM-12:00PM UTC / 11:00AM-1:00PM CET

In this workshop we will focus on how you can secure your application with GitLab. 

We will first take a look at how to apply scanners to your CICD pipelines in a hands-on exercise so that any vulnerabilities are caught as soon as the code is committed. 

Next, we will look at compliance frameworks and pipelines to show how you can ensure no one within your development teams is cutting corners and exposing your application.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_0B6V3eGiSLSZ21I3uDyaHw#/registration)

#### Security and Compliance Webinar
##### November 28th, 2023 at 9:00-10:00AM UTC / 10:00-11:00AM CET

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_FywMG2KZQoKIpdvq0D7eIw#/registration)

#### AI in DevSecOps - GitLab Hands-On Workshop
##### November 29th, 2023 at 10:00AM-11:30AM UTC / 11:00AM-12:30PM CET

Join us for a hands-on GitLab AI workshop where we will explore Artificial Intelligence and how it fits within the DevSecOps lifecycle. In this session, we will cover foundational implementation and use case scenarios such as Code Suggestions and Vulnerability Explanations.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_JIipJ0KeQxSmktwHzx8kJw#/registration)

#### Getting Started with DevOps Metrics
##### November 30th, 2023 at 9:00-10:00AM UTC / 10:00-11:00AM CET

Come learn about DevOps metrics in GitLab and why it is useful to track them. We will cover an overview of DORA metrics, Value Stream Analytics, and Insight dashboards, and what it looks like in Gitlab.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_6UVCDzoZReSZJ4z-pUVscQ#/registration)

### APAC Time Zone Webinars

#### Advanced CI/CD
##### November 16th, 2023 at 10:30AM-11:30AM India / 12:00-1:00PM Indonesia / 1:00-2:00PM Singapore / 4:00-5:00PM Sydney

Expand your CI/CD knowledge while we cover advanced topics that will accelerate your efficiency using GitLab, such as pipelines, variables, rules, artifacts, and more. This session is intended for those who have used CI/CD in the past.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_k_P3HkhSTGmBGqxub0D-DQ#/registration)

#### Security and Compliance
##### November 21st, 2023 at 10:30AM-11:30AM India / 12:00-1:00PM Indonesia / 1:00-2:00PM Singapore / 4:00-5:00PM Sydney

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_3KwvMBoJQlmkxltx3bXBXA#/registration)


Check back later for more webinars! 
