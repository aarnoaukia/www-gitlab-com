---
layout: handbook-page-toc
title: "CSM Responsibilities and Services"
description: "There are various services a Customer Success Manager will provide to ensure that customers get the best value possible out of their relationship with GitLab."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [CSM Handbook homepage](/handbook/customer-success/csm/) for additional CSM-related handbook pages.

---

## CSM Alignment

For an overview of CSM segments by ARR, please see [internal handbook](https://internal.gitlab.com/handbook/customer-success/csm/#csm-segments)(Internal GitLab only)

## Pillars of Success
There are 7 pillars that we have identified as crucial for success as CSMs. It is expected and normal for all CSMs to have higher proficiency in some areas and lower in others, especially when taking [job level](/company/team/structure/#layers) into consideration, but these are all areas we are continually striving to be better at, as there are always opportunities to learn and improve.
- Communication
  - Frequent and honest communication with customers and internal stakeholders, so everyone knows what is happening and what to expect, in line with [GitLab communication guidelines](/handbook/communication/)
- Follow-through
  - You never leave a customer without an answer/reply, and you always do what you’ve said you’ll do
- Organization & Documentation
  - You have your tasks organized and prioritized, and it’s easy for someone to quickly understand what’s going on with any given account by looking at Gainsight
- Relationship-building
  - Both customers and internal team members trust you
- Proactivity
  - You don’t wait to be told what to do, you have [short toes](/handbook/values/#short-toes) and a [bias for action](/handbook/values/#bias-for-action), and you plan for customer strategy in collaboration with your go-to-market team.
- Assertiveness
  - You are comfortable telling customers what they need to do as their trusted advisor, as well as working internally to get what you need from others
- Perceptiveness
  - You can “read between the lines” of what a customer is saying and identify what they need, not necessarily what they say they want

## Responsibilities and Services

There are various services a Customer Success Manager will provide to ensure that customers get the best value possible out of their relationship with GitLab and their utilization of GitLab's products and services. These services typically include the following. Please note this list is not definitive, and more services may be provided than listed or some may not be offered, depending on the size and details of the account.

### Relationship Management

- Scheduled cadence calls (synchronous)
- Regular open issue reviews and issue escalations (asynchronous)
- Success strategy roadmaps - beginning with an onboarding success plan, for example a 30/60/90 day plan
- Focus on adoption roadmap and milestones in-line with desired business outcomes and intended use cases.
- Executive business reviews (1-2 times a year for Enterprise; as needed for Commercial)
- Partnership into expansion into new use cases and associated enablement & guidance
- Internal Advocacy: CSM is the customer champion for guidance and requests, a liaison between the customer and other GitLab teams

### Assisting with Licensing & Subscriptions

Customer Success Managers can provide assistance to customers that qualify for CSM management in:
  - Ensuring new & renewing customers understand how to locate the [customer portal](https://customers.gitlab.com) and
    are successful in applying their license or subscription to their instance or namespace
  - Aiding cross-functional team members in understanding the information available via the [admin interface of the customer portal](https://customers.gitlab.com/admin)
  - Providing insight into seat utilization and/or overages during renewal or add-on discussions

Isues that may arise during renewal or onboarding can be most commonly addressed by opening an [internal request](/handbook/support/internal-support/#internal-requests) with [GitLab
Support's Licensing & Renewals team](https://handbook.gitlab.com/handbook/support/license-and-renewals/).

Questions about deal structure, salesforce opportunities or other commercially-impacting aspects, should be handled by Sales and directed via [Sales Support](/handbook/sales/field-operations/order-processing/)

For more information & enablement on GitLab's Licensing process, here are resources to explore:
  - [Licensing & Subscription FAQ](https://about.gitlab.com/pricing/licensing-faq/)
  - [Post sale License provisioning (SaaS and Self-Managed)](https://internal.gitlab.com/handbook/product/fulfillment/provision/#purchase-to-login-provisioning-step-by-step-process) Internal Handbook, step by step guide of the Customer experience after a purchase  
  - [Post sale Licensing - AE perspective](/handbook/sales/field-operations/order-processing/#post-sale-information) Public page, documenting common sales troubleshooting
  - [Cloud Licensing Handbook Page](https://about.gitlab.com/pricing/licensing-faq/cloud-licensing/)
  - [Internal Cloud Licensing page](https://internal.gitlab.com/handbook/product/fulfillment/cloudlicensing/)
  - [Support's Licensing & Renewals Workflows](https://handbook.gitlab.com/handbook/support/license-and-renewals/workflows/) (good for confirming if an issue you're facing can be solved through support)
  - [Highspot Landing Page for Sales Enablement on Cloud Licensing](https://gitlab.highspot.com/items/62506b2d785b7cc84139d48a#6)
  - For specific questions about licensing or subscription management, questions can be asked in slack via [#s_fulfillment](https://gitlab.slack.com/archives/CMJ8JR0RH)

Generally speaking, customer trials can be activated on a self-service basis through [https://about.gitlab.com/free-trial/](https://about.gitlab.com/free-trial/)

### Training

- Identification of pain points and training required
- Coordination of demos and training sessions, potentially delivered by the Customer Success Manager if time and technical knowledge allows
- Deliver the use case enablement sessions/workshops/lunch & learns to ensure the teams, leadership and end users are setup for success and adopting the platform successfully 
- Regular communication and updates on GitLab features
- Product and feature guidance - new feature presentations

### Working with Customer Support Engineering

- Upgrade planning (in partnership with Support)
  - Review the [Upgrade Assistance page](/support/scheduling-upgrade-assistance/) with customers during upgrade planning to ensure a plan is in place (including a rollback strategy) and Support have enough time to review the plan
- [Infrastructure upgrade coordination](/handbook/customer-success/csm/services/infrastructure-upgrade/) -  CSMs may provide high-level guidance but the technical implementation should ideally be provided by Professional Services via [Dedicated Implementation Services](/services/implementation/enterprise/)
- Launch best practices
- Review and submit [Support Ticket Attention Requests](/handbook/support/internal-support/support-ticket-attention-requests.html)
- Monitor SaaS based customer experience by adding them to the [Marquee Accounts alerts](https://gitlab.com/gitlab-com/gl-infra/marquee-account-alerts) project
