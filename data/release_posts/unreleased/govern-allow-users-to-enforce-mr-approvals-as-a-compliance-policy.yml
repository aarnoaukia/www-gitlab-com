---
features:
  primary:
  - name: "Allow users to enforce MR approvals as a compliance policy"
    available_in: [ultimate]
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/user/application_security/policies/scan-result-policies.html#any_merge_request-rule-type'
    image_url: '/images/unreleased/security-policies-enforce-two-person-approval.png'
    reporter: g.hickman
    stage: govern
    categories: 
    - Security Policy Management
    epic_url: 'https://gitlab.com/groups/gitlab-org/-/epics/9696'
    description: |
      There is an increasing scrutiny on code changes that can potentially land in production applications and open businesses up to compliance risk and security vulnerability. With scan result policies, you can ensure unilateral changes cannot be made by enforcing two person approval on all merge requests.

      Scan results policies have a new option to target `Any merge request` which can be paired with defining [role-based approvers](https://docs.gitlab.com/ee/user/application_security/policies/scan-result-policies.html#require_approval-action-type) to ensure each MR for the defined branches require approval by two (or more) users with a given role (Owner, Maintainer, or Developer).
