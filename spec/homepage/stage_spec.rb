describe Gitlab::Homepage::Stage do
  subject(:stage) { described_class.new(key, data) }
  let(:key) { 'manage' }
  let(:data) do
    {
      'display_name' => 'Manage',
      'groups' => {
        'access' => {},
        'import' => {}
      },
      'slack' => { 'channel' => 's_create' }
    }
  end

  describe '#method_missing' do
    it 'returns value by key' do
      expect(stage.display_name).to eq('Manage')
    end

    context 'when key is missing' do
      it { expect(stage.unknown).to be_nil }
    end
  end

  describe '#groups' do
    subject { stage.groups }

    it 'makes a call to Category#for_stage' do
      expect(subject).to eq(%w[access import])
    end
  end

  describe '#categories' do
    subject { stage.categories }

    it 'makes a call to Category#for_stage' do
      expect(Gitlab::Homepage::Category).to receive(:for_stage).with(stage)

      subject
    end
  end

  describe '#label' do
    subject { stage.label }

    context 'when name is defined' do
      it { is_expected.to eq("#{described_class::LABEL_PREFIX}manage") }
    end

    context 'when label is defined' do
      let(:label_name) { 'stage::manage stage' }
      let(:data) { { 'label' => label_name } }

      it { is_expected.to eq(label_name) }
    end
  end

  describe '#slack' do
    subject { stage.slack }

    context 'when slack.channel is defined' do
      it { is_expected.to eq('s_create') }
    end

    context 'when slack is not defined' do
      let(:data) { {} }

      it { is_expected.to be_nil }
    end

    context 'when slack.channel is not defined' do
      let(:data) { { 'slack' => {} } }

      it { is_expected.to be_nil }
    end
  end

  describe '.all!' do
    subject { described_class.all! }

    before do
      allow(YAML).to receive(:load_file) do
        {
          'stages' => {
            'manage' => { 'display_name' => 'Manage' },
            'plan' => { 'display_name' => 'Plan' }
          }
        }
      end
    end

    it 'returns Stage objects' do
      stage = subject

      expect(stage.count).to eq(2)
      expect(stage.map(&:display_name)).to match_array(%w[Manage Plan])
    end
  end
end
